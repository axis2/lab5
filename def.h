#ifndef LAB1_DEF_H
#define LAB1_DEF_H

#include <stdio.h>
#include <stdbool.h>


#define errorf(Format, ...)         fprintf(stderr, Format, ##__VA_ARGS__)

#define NO_ERROR                    0

#define forever                     while (true)

#endif //LAB1_DEF_H
