cmake_minimum_required(VERSION 3.16)
project(lab5 C)

add_compile_options(-lpthread)

set(CMAKE_C_STANDARD 99)

add_executable(lab5 main.c thread_util.c)