#ifndef THREAD_UTIL_H
#define THREAD_UTIL_H

#include <pthread.h>

#include "def.h"


int start_thread(
        pthread_t *thread,
        pthread_attr_t *attr,
        void *(fun)(void *),
        void *args);

int join_thread(
        pthread_t thread,
        void **res);

int cancel_thread(pthread_t thread);

#endif //THREAD_UTIL_H
