#include <string.h>

#include "thread_util.h"


#define DEFINE_ERROR_REPORTING_WRAPPER(WrapperSignature, WrappedCall, ErrorMessageString) \
WrapperSignature {\
    int errcode = WrappedCall;\
    if (errcode != NO_ERROR) {\
        errorf(ErrorMessageString ": %s\n", strerror(errcode));\
    }\
    return errcode;\
}


DEFINE_ERROR_REPORTING_WRAPPER(
        int start_thread(
                pthread_t *thread,
                pthread_attr_t *attr,
                void *(fun)(void *),
                void *args
        ),
        pthread_create(thread, attr, fun, args),
        "failed to spawn a thread"
)


DEFINE_ERROR_REPORTING_WRAPPER(
        int join_thread(
                pthread_t thread,
                void **res
        ),
        pthread_join(thread, res),
        "failed to join a thread"
)


DEFINE_ERROR_REPORTING_WRAPPER(
        int cancel_thread(pthread_t thread),
        pthread_cancel(thread),
        "failed to join a thread"
)
