#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "thread_util.h"

void printDeathRattle(void *_) {
    printf("i'm being cancelled\n");
}

void *printForever(void *_) {
    pthread_cleanup_push(printDeathRattle, NULL);
    unsigned line_no = 0;

    forever {
        printf("line #%u\n", line_no++);
        pthread_testcancel();
    }

    pthread_cleanup_pop(false);

    return NULL;
}

#define WAIT_TIME_S                 2

int main() {
    int errcode;

    pthread_t thread;
    errcode = start_thread(&thread, NULL, printForever, NULL);
    if (errcode != NO_ERROR) return errcode;

    sleep(WAIT_TIME_S);

    cancel_thread(thread);
    join_thread(thread, NULL);

    return EXIT_SUCCESS;
}
